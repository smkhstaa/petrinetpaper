package info.scce.cinco.product.petrinetpaper.actions

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.cinco.product.petrinetpaper.petrinet.Transition

class TriggerTransitionX  extends CincoCustomAction<Transition> {	
		
	override canExecute(Transition transition) {		
		transition.predecessors.filter[token == 0].empty		
	}

	override execute(Transition transition) {
		transition.predecessors.forEach[token = token - 1]
		transition.successors.forEach[token = token + 1]
				
	}

	override getName() {
		"Trigger Transition (Xtend Implementation)"
	}

}