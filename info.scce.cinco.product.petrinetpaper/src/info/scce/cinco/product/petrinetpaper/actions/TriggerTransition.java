package info.scce.cinco.product.petrinetpaper.actions;

import de.jabc.cinco.meta.runtime.action.CincoCustomAction;
import info.scce.cinco.product.petrinetpaper.petrinet.Place;
import info.scce.cinco.product.petrinetpaper.petrinet.Transition;

public class TriggerTransition extends CincoCustomAction<Transition>{
	
	public boolean canExecute(Transition transition) {
		for (Place p : transition.getPredecessors()) {
			if (p.getToken() == 0)
				// can't execute if one predecessor with 0 tokens is found
				return false;
		}
		return true;
	}

	public void execute(Transition transition) {
		// decrement each predecessor by 1
		for (Place pre : transition.getPredecessors()) {
			pre.setToken(pre.getToken()-1);
		}
		// increment each successor by 1
		for (Place succ : transition.getSuccessors()) {
			succ.setToken(succ.getToken()+1);
		}
	}

	public String getName() {
		return "Trigger Transition";
	}

}